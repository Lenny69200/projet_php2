<?php

namespace Entities;

class Competition
{
private $idCompet;
private $dateDebutCompet;
private $dateFinCompet;
private $idClubOrganisateur;

    /**
     * @return mixed
     */
    public function getIdCompet()
    {
        return $this->idCompet;
    }

    /**
     * @param mixed $idCompet
     */
    public function setIdCompet($idCompet): void
    {
        $this->idCompet = $idCompet;
    }



    /**
    /**
     * @return mixed
     */
    public function getDateDebutCompet()
    {
        return $this->dateDebutCompet;
    }

    /**
     * @param mixed $dateDebutCompet
     */
    public function setDateDebutCompet($dateDebutCompet): void
    {
        $this->dateDebutCompet = $dateDebutCompet;
    }

    /**
     * @return mixed
     */
    public function getDateFinCompet()
    {
        return $this->dateFinCompet;
    }

    /**
     * @param mixed $dateFinCompet
     */
    public function setDateFinCompet($dateFinCompet): void
    {
        $this->dateFinCompet = $dateFinCompet;
    }




    /**
     * @return mixed
     */
    public function getIdClubOrganisateur():?int
    {
        return $this->idClubOrganisateur;
    }


    public function setIdClubOrganisateur($idClubOrganisateur): void
    {
        $this->idClubOrganisateur = $idClubOrganisateur;
    }


    function __construct(?array $datas = null) {
        if ($datas !== null){
            (isset($datas['idCompet'])) ? $this->idCompet = $datas['idCompet']: $this->getIdCompet(null);
            (isset($datas['dateDebutCompet'])) ? $this->dateDebutCompet = $datas['dateDebutCompet']: $this->getDateDebutCompet(null);
            (isset($datas['dateFinCompet'])) ? $this->dateFinCompet = $datas['dateFinCompet']: $this->getDateFinCompet(null);
            (isset($datas['idClubOrganisateur'])) ? $this->idClubOrganisateur = $datas['idClubOrganisateur']: $this->getIdClubOrganisateur(null);
        }

    }




}