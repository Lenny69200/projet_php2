<?php

namespace Entities;

class NiveauTireur
{

    private $idNivTireur;

private $libNivTireur;


    public function getIdNivTireur()
    {
        return $this->idNivTireur;
    }

    public function setIdNivTireur($idNivTireur): void
    {
        if($this->idNivTireur == null){
            $this->idNivTireur = $idNivTireur;

        }    }
    /**
     * @return mixed
     */
    public function getLibNivTireur()
    {
        return $this->libNivTireur;
    }

    /**
     * @param mixed $libNivTireur
     */
    public function setLibNivTireur($libNivTireur): void
    {
        $this->libNivTireur = $libNivTireur;
    }



    function __construct(?array $datas = null) {
        if ($datas !== null){
            (isset($datas['idNivTireur'])) ? $this->idNivTireur = $datas['idNivTireur']: $this->getIdNivTireur(null);
            (isset($datas['libNivTireur'])) ? $this->libNivTireur = $datas['libNivTireur']: $this->getLibelNivTireur(null);

        }

    }


}