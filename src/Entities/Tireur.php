<?php

namespace Entities;

class Tireur
{
    private $idTireur;
    private $nomTireur;
    private $prenomTireur;
    private $dateNaissTireur;
    private $numLicenceTireur;
    private $sexeTireur;
    private $poidsTireur;
    private $idClub;
    private $idNivTireur;




    public function getPoidsTireur() : float
    {
        return $this->poidsTireur;
    }


    public function setPoidsTireur($poidsTireur): void
    {
        $this->poidsTireur = $poidsTireur;
    }

    public function getIdTireur() :?int
    {
        return $this->idTireur;
    }


    public function setIdTireur($idTireur): void
    {
        if($this->idTireur == null){
            $this->idTireur = $idTireur;

        }
    }


    public function getNomTireur() : string
    {
        return $this->nomTireur;
    }

    public function setNomTireur($nomTireur): void
    {
        $this->nomTireur = $nomTireur;
    }


    public function getPrenomTireur() :string
    {
        return $this->prenomTireur;
    }


    public function setPrenomTireur($prenomTireur): void
    {
        $this->prenomTireur = $prenomTireur;
    }


    public function getDateNaissTireur() : ?string
    {
        return $this->dateNaissTireur;
    }


    public function setDateNaissTireur($dateNaissTireur): void
    {
        $this->dateNaissTireur = $dateNaissTireur;
    }


    public function getSexeTireur() : string
    {
        return $this->sexeTireur;
    }


    public function setSexeTireur($sexeTireur): void
    {
        $this->sexeTireur = $sexeTireur;
    }


    public function getNumLicenceTireur() : int
    {
        return $this->numLicenceTireur;
    }


    public function setNumLicenceTireur($numLicenceTireur): void
    {
        $this->numLicenceTireur = $numLicenceTireur;
    }

    /**
     * @return mixed
     */
    public function getIdClub() :?int
    {
        return $this->idClub;
    }

    /**
     * @param mixed $idClub
     */
    public function setIdClub($idClub): void
    {
        $this->idClub = $idClub;
    }


    public function getIdNivTireur() : ?int
    {
        return $this->idNivTireur;
    }


    public function setIdNivTireur($idNivTireur): void
    {
        $this->idNivTireur = $idNivTireur;
    }


    function __construct(?array $datas = null) {
        if ($datas !== null){
            (isset($datas['idTireur'])) ? $this->idTireur = $datas['idTireur']: $this->getIdTireur(null);
            (isset($datas['nomTireur'])) ? $this->nomTireur = $datas['nomTireur']: $this->getNomTireur('');
            (isset($datas['prenomTireur'])) ? $this->prenomTireur = $datas['prenomTireur']: $this->getPrenomTireur('');
            (isset($datas['dateNaissTireur'])) ? $this->dateNaissTireur = $datas['dateNaissTireur']: $this->getDateNaissTireur('');
            (isset($datas['numLicenceTireur'])) ? $this->numLicenceTireur = $datas['numLicenceTireur']: $this->getNumLicenceTireur(null);
            (isset($datas['sexeTireur'])) ? $this->sexeTireur = $datas['sexeTireur']: $this->getSexeTireur('');
            (isset($datas['poidsTireur'])) ? $this->poidsTireur = $datas['poidsTireur']: $this->getPoidsTireur(null);
            (isset($datas['idClub'])) ? $this->idClub = $datas['idClub']: $this->getIdClub(null);
            (isset($datas['idNivTireur'])) ? $this->idNivTireur = $datas['idNivTireur']: $this->getIdNivTireur(null);
        }

    }

    }