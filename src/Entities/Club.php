<?php
namespace Entities;

class Club
{

    private  $idClub;
    private  $nomClub;
    private $adresseClub;
    private $cpClub;
    private $villeClub;

    /**
     getter
     */
    public function getVilleClub(): string
    {
        return $this->villeClub;
    }

    public function getCpClub():string
    {
        return $this->cpClub;
    }

    public function getAdresseClub() :string
    {
        return $this->adresseClub;
    }

    public function getNomClub():string
    {
        return $this->nomClub;
    }

    public function getIdClub(): ?int
    {
        return $this->idClub;
    }

    /**
     setter
     */
    public function setIdClub(int $idClub): void
    {
if($this->idClub == null){
    $this->idClub = $idClub;

}
    }

    public function setNomClub(string $nomClub): void
    {
        $this->nomClub = $nomClub;
    }

    public function setAdresseClub(string $adresseClub): void
    {
        $this->adresseClub = $adresseClub;
    }

    public function setCpClub(string $cpClub): void
    {
        $this->cpClub = $cpClub;
    }

    public function setVilleClub(string $villeClub): void
    {
        $this->villeClub = $villeClub;

    }

    function __construct(?array $datas = null) {
       if ($datas !== null){
           (isset($datas['idClub'])) ? $this->idClub = $datas['idClub']: $this->getIdClub(null);
           (isset($datas['nomClub'])) ? $this->nomClub = $datas['nomClub']: $this->getNomClub(null);
           (isset($datas['adresseClub'])) ? $this->adresseClub = $datas['adresseClub']: $this->getAdresseClub(null);
           (isset($datas['cpClub'])) ? $this->cpClub = $datas['cpClub']: $this->getCpClub(null);
           (isset($datas['villeClub'])) ? $this->villeClub = $datas['villeClub']: $this->getVilleClub(null);
       }

    }


}