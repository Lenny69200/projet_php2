<?php

namespace Repositories;
use Entities\Club;

class ClubRepository
{

    protected $bdd;

    public function __construct(\PDO $bdd){
        if(!is_null($bdd))
            $this->bdd = $bdd;
    }

    public function getAll() : ? array {

        $resultSet = NULL;
        $query = 'SELECT * FROM Club';
        dump_var($query, DUMP, 'Requête SQL:');


        $rqtResult = $this->bdd->query($query);


        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row ) {
                $resultSet[] = new Club($row);

            }
        }

        return $resultSet;
    }


    public function getById( int $id): ?Club {
        $resultSet = NULL;
        $query = 'SELECT * FROM Club WHERE idClub=:idClub;';

        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':idClub' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                $resultSet = new Club($tab);
            }
        }
        return $resultSet;
    }

    public function insert(Club $entity): ?Club {
        $resultSet = NULL;


        $query = "INSERT INTO Club" .
            " (nomClub, adresseClub, cpClub,villeClub)"
            . " VALUES (:nomClub, :adresseClub, :cpClub,:villeClub)";

        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute(
            [':nomClub' => $entity->getNomClub(),
                ':adresseClub' => $entity->getadresseClub(),
                ':cpClub' => $entity->getcpClub(),
                ':villeClub'=>$entity->getVilleClub(),
            ]
        );

        if ($res !== FALSE) {
            $entity->setIdClub($this->bdd->lastInsertId());
            $resultSet = $entity;
        }

        return $resultSet;
    }

    public function update(Club $entity): ?Club {
        $resultSet = NULL;
        if (is_null($entity->getIdClub()) || is_null($this->getById($entity->getIdClub()))) {
            $resultSet = NULL;
        } else {
            //  Entité existante
            $query = "UPDATE Club"
                . " SET nomClub=:nomClub, "
                . " adresseClub=:adresseClub,"
                . " cpClub=:cpClub,"
                ."villeClub=:villeClub"
                . " WHERE idClub = :idClub";

            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update Formation');
            $res = $reqPrep->execute(
                [':idClub' => $entity->getIdClub(),
                    ':nomClub' => $entity->getNomClub(),
                    ':adresseClub' => $entity->getAdresseClub(),
                    'cpClub'=>$entity->getCpClub(),
                    ':villeClub' => $entity->getVilleClub()
                ]
            );

            if ($res !== FALSE) {
                $resultSet = $entity;
            }
        }

        return $resultSet;
    }

    public function save(Club $entity): ?Club{
        $resultSet = NULL;
    if ($entity->getIdClub()==null){
        insertClub();
    }else{
        updateClub();
    }
    return $resultSet;
    }

}