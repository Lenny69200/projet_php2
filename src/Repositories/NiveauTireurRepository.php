<?php

namespace Repositories;


use Entities\NiveauTireur;

class NiveauTireurRepository
{

    protected $bdd;

    public function __construct(\PDO $bdd){
        if(!is_null($bdd))
            $this->bdd = $bdd;
    }

    public function getAll() : ? array {

        $resultSet = NULL;
        $query = 'SELECT * FROM NiveauTireur';
        dump_var($query, DUMP, 'Requête SQL:');


        $rqtResult = $this->bdd->query($query);


        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row ) {
                $resultSet[] = new NiveauTireur($row);

            }
        }

        return $resultSet;
    }
    public function getById( int $id): ?NiveauTireur {
        $resultSet = NULL;
        $query = 'SELECT * FROM NiveauTireur WHERE idNivTireur=:idNivTireur;';

        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':idNivTireur' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                $resultSet = new NiveauTireur($tab);
            }
        }
        return $resultSet;
    }


    public function insert(NiveauTireur $entity): ?NiveauTireur {
        $resultSet = NULL;


        $query = "INSERT INTO NiveauTireur 
            (libNivTireur) VALUES (:libNivTireur)";

        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute(
            [':libNivTireur' => $entity->getLibNivTireur(),
            ]
        );

        if ($res !== FALSE) {
            $entity->setIdNivTireur($this->bdd->lastInsertId());
            $resultSet = $entity;
        }

        return $resultSet;
    }

    public function update(NiveauTireur $entity): ?NiveauTireur {
        $resultSet = NULL;
        if (is_null($entity->getIdNivTireur()) || is_null($this->getById($entity->getIdNivTireur()))) {
            $resultSet = NULL;
        } else {
            //  Entité existante
            $query = "UPDATE NiveauTireur"
                . " SET libNivTireur=:libNivTireur"
                . " WHERE idNivTireur = :idNivTireur";

            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update NiveauTireur');
            $res = $reqPrep->execute(
                [':idNivTireur' => $entity->getIdNivTireur(),
                    ':libNivTireur' => $entity->getLibNivTireur()
                ]
            );

            if ($res !== FALSE) {
                $resultSet = $entity;
            }
        }

        return $resultSet;
    }
}