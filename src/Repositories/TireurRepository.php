<?php

namespace Repositories;
use Entities\NiveauTireur;
use Entities\Tireur;

class TireurRepository
{
    protected $bdd;

    public function __construct(\PDO $bdd){
        if(!is_null($bdd))
            $this->bdd = $bdd;
    }

    public function getAll() : ? array {

        $resultSet = NULL;
        $query = 'SELECT * FROM Tireur';
        dump_var($query, DUMP, 'Requête SQL:');


        $rqtResult = $this->bdd->query($query);


        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row ) {
                $resultSet[] = new Tireur($row);

            }
        }

        return $resultSet;
    }

    public function getById( int $id): ?Tireur {
        $resultSet = NULL;
        $query = 'SELECT * FROM Tireur WHERE idTireur=:idTireur;';

        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':idTireur' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                $resultSet = new Tireur($tab);
            }
        }
        return $resultSet;
    }

    public function insert(Tireur $entity): ?Tireur {
        $resultSet = NULL;
        dump_var($entity, DUMP, 'Contenu de mon entity avant insert');

        $query = "INSERT INTO Tireur 
            (nomTireur,prenomTireur,dateNaissTireur,numLicenceTireur,sexeTireur,poidsTireur,idClub,idNivTireur) 
            VALUES (:nomTireur,:prenomTireur,:dateNaissTireur,:numLicenceTireur,:sexeTireur,:poidsTireur,:idClub,:idNivTireur)";

        $reqPrep = $this->bdd->prepare($query);
        dump_var($reqPrep, DUMP, '$reqPrep dans insert Tireur');
        $res = $reqPrep->execute(
            [':nomTireur' => $entity->getNomTireur(),
                ':prenomTireur' => $entity->getPrenomTireur(),
                ':dateNaissTireur' => $entity->getDateNaissTireur(),
                ':numLicenceTireur' => $entity->getNumLicenceTireur(),
                ':sexeTireur' => $entity->getSexeTireur(),
                ':poidsTireur' => $entity->getPoidsTireur(),
                ':idClub' => $entity->getIdClub(),
                ':idNivTireur' => $entity->getIdNivTireur(),
            ]
        );

        if ($res !== FALSE) {
            $entity->setIdTireur($this->bdd->lastInsertId());
            $resultSet = $entity;
        }

        return $resultSet;
    }


    public function update(Tireur $entity): ?Tireur {
        $resultSet = NULL;
        if (is_null($entity->getIdTireur()) || is_null($this->getById($entity->getIdTireur()))) {
            $resultSet = NULL;
        } else {
            //  Entité existante
            $query = "UPDATE Tireur"
                . " SET nomTireur=:nomTireur, "
                . "prenomTireur=:prenomTireur, "
                . "dateNaissTireur=:dateNaissTireur, "
                . "numLicenceTireur=:numLicenceTireur, "
                . "sexeTireur=:sexeTireur, "
                . "poidsTireur=:poidsTireur, "
                . "idClub=:idClub, "
                . "idNivTireur=:idNivTireur"
                . " WHERE idTireur = :idTireur";

            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update Tireur');
            $res = $reqPrep->execute(
                [':idTireur' => $entity->getIdTireur(),
                    ':nomTireur' => $entity->getNomTireur(),
                    ':prenomTireur' => $entity->getPrenomTireur(),
                    ':dateNaissTireur' => $entity->getDateNaissTireur(),
                    ':numLicenceTireur' => $entity->getNumLicenceTireur(),
                    ':sexeTireur' => $entity->getSexeTireur(),
                    ':poidsTireur' => $entity->getPoidsTireur(),
                    ':idClub' => $entity->getIdClub(),
                    ':idNivTireur' => $entity->getIdNivTireur(),

                ]
            );

            if ($res !== FALSE) {
                $resultSet = $entity;
            }
        }

        return $resultSet;
    }
}