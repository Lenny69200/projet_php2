<?php

namespace Repositories;

use Entities\Competition;

class CompetitionRepository
{
    protected $bdd;

    public function __construct(\PDO $bdd)
    {
        if (!is_null($bdd))
            $this->bdd = $bdd;
    }

    public function getAll(): ?array
    {

        $resultSet = NULL;
        $query = 'SELECT * FROM competition';
        dump_var($query, DUMP, 'Requête SQL:');


        $rqtResult = $this->bdd->query($query);


        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach ($rqtResult as $row) {
                $resultSet[] = new competition($row);

            }
        }

        return $resultSet;
    }

    public function getById(int $id): ?competition
    {
        $resultSet = NULL;
        $query = 'SELECT * FROM Competition WHERE idCompet=:idCompet;';

        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':idCompet' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if (!is_null($tab)) {
                $resultSet = new Competition($tab);
            }
        }
        return $resultSet;
    }

    public function insert(Competition $entity): ?Competition
    {
        $resultSet = NULL;


        $query = "INSERT INTO competition (dateDebutCompet,dateFinCompet,idClubOrganisateur) VALUES (:dateDebutCompet,:dateFinCompet,:idClubOrganisateur)";

        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute(
            [':dateDebutCompet' => $entity->getDateDebutCompet(),
                ':dateFinCompet' => $entity->getDateFinCompet(),
                ':idClubOrganisateur' => $entity->getIdClubOrganisateur(),
            ]
        );

        if ($res !== FALSE) {
            $entity->setIdCompet($this->bdd->lastInsertId());
            $resultSet = $entity;
        }

        return $resultSet;
    }

    public function update(Competition $entity): ?Competition
    {
        $resultSet = NULL;
        if (is_null($entity->getIdCompet()) || is_null($this->getById($entity->getIdCompet()))) {
            $resultSet = NULL;
        } else {
            //  Entité existante
            $query = "UPDATE competition"
                . " SET dateDebutCompet=:dateDebutCompet,"
                . "dateFinCompet=:dateFinCompet,"
                . "idClubOrganisateur=:idClubOrganisateur"
                . " WHERE idCompet=:idCompet";

            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update Competition');
            $res = $reqPrep->execute(
                [':idCompet' => $entity->getIdCompet(),
                    ':dateDebutCompet' => $entity->getDateDebutCompet(),
                    ':dateFinCompet' => $entity->getDateFinCompet(),
                    ':idClubOrganisateur' => $entity->getIdClubOrganisateur()
                ]
            );

            if ($res !== FALSE) {
                $resultSet = $entity;
            }
        }

        return $resultSet;
    }

    public function delete(int $entity): void
    {
        $resultSet = NULL;

        $query = 'SELECT idRencontre FROM rencontre WHERE idCompet=:idCompet;';

        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':idCompet' => $entity]);
        var_dump($res);
        if ($res) {
            $reqPrep->setFetchMode(\PDO::FETCH_ASSOC);
            foreach ($reqPrep as $row) {
                $resultSet[] = $row;
                var_dump($row);
            }
            var_dump($resultSet);
        }
        var_dump($entity);
        if ($resultSet != null) {
            foreach ($resultSet as $row) {
                $query = 'Delete  FROM participer WHERE idRencontre=:idRencontre;';

                $reqPrep = $this->bdd->prepare($query);

                $res = $reqPrep->execute([':idRencontre' => $row['idRencontre']]);

                $query = 'Delete  FROM rencontre WHERE idRencontre=:idRencontre;';

                $reqPrep = $this->bdd->prepare($query);

                $res = $reqPrep->execute([':idRencontre' => $row ['idRencontre']]);

            }
            $query = 'Delete FROM Competition WHERE idCompet=:idCompet;';

            $reqPrep = $this->bdd->prepare($query);

            $res = $reqPrep->execute([':idCompet' => $entity]);

            var_dump($resultSet);
        }
    }
}