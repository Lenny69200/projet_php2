<?php

/*
 * Exercice 1 Question 2 : Faire la fonction pour ce connecter à la BDD
 * Crée la connexion PDO de votre application
 * @param array $infoBdd tableau qui contient les informations de connexion de la BDD, c'est celui du fichier appConfig
 * @return PDO Un objet de type PDO
 */
function connectBdd(array $infoBdd): ?\PDO {
    $db = null;
    // Récupération des informations de votre table infoBdd, il fortement conseillé d'utiliser des variables, la première vous est donnée
    $myport = ($infoBdd['port']);
    $mycharset = ($infoBdd['charset']);
    $hostname = ($infoBdd['host']);
    $mydbname = ($infoBdd['dbname']);
    $myusername = ($infoBdd['user']);
    $mypassword = ($infoBdd['pass']);




    //  Composition du DSN
$dsn = "mysql:dbname=$mydbname;host=$hostname;port=$myport;charset=$mycharset";
    //  Instanciation de PDO (le \ pour l'espace de nom racine... peut être utile par la suite)
$db = new \PDO($dsn,$myusername,$mypassword);
    // renvoi de votre object PDO
    return $db;
}


/*
 * Exercice 2 - Question 1 : Faire la fonction getAllClubs qui permet de récupèrer l'ensemble des clubs présent en BDD
 * Récupère tous les clubs depuis la BDD
 * @param PDO $bdd
 * @return array|null Un tableau indéxé de tableaux associatifs
 */
function getAllClubs(\PDO $bdd): ?array {
    $resultSet = NULL;
    // Créer la requête SQL qui va permettre de récupérer tous les clubs
    $query = 'Select * from club';
    //Exécution de la rêquete
    $res = $bdd->query($query);
    //Si votre requête renvoie quelque chose, parcourez le résultat et insérer le dans $resultSet;
    if($res){
        while ($row = $res->fetch(\PDO::FETCH_ASSOC)){
            $resultSet[]= $row;
        }
    }
    // renvoi de votre tableau contenant l'ensemble des clubs
    return $resultSet;
}

/*
 * Exercice 3 - Question 1
 * Insère un nouveau club dans la bdd
 * @param PDO $bdd
 * @param array $club tableau normalement sans clé id
 * @return array|null le tableau complété avec l'id
 */
function insertClub(\PDO $bdd, array $club): ?array {
    dump_var($club, DUMP, '$club dans insertClub');
    $resultSet = NULL;
    // Créer votre requête avec quote, on ne s'occupe pas des informations qui viennent des clés étrangère et on laisse l'auto_incremente se gérer de la clé primaire
    $query = "Insert INTO CLUB (NomCLub,AdresseClub,CpClub,VilleClub) values ('".$club['nomClub']."','".$club['adresseClub']."','".$club['cpClub']."','".$club['villeClub']."');";
    //Vérifier votre req
   dump_var($query,DUMP,'$query dans insertClub');
    //Exécuter votre requête
$bdd->exec($query);

    //Récupèrer l'id généré et l'ajouter à votre tableau

    $id = $bdd->lastInsertId();

    //Ajout de l'id du club dans notre tableau $club
echo $id;
    $club['idClub'] = $id;
    //retourner le tableau maj avec l'id
    return $club;
}

/*
 * Exercice 4 - Question 1
 * Récupère le club à partir du n° id
 * @param PDO $bdd
 * @param int $id
 * @return array|null
 */
function getClubId(PDO $bdd, int $id): ?array {
    $resultat = NULL;
    // Créer votre requête en utilisant une requête préparée
    $query = sprintf("SELECT * FROM CLUB WHERE idClub=%s", $id);
    //préparer votre requête
    $reqPrep = $bdd->prepare($query);
    dump_var($reqPrep, DUMP, '$reqPrep dans getOeuvreId');
    // Exécution de votre requête

    $reqPrep->execute();

    //Récupération du résultat dans votre resultSet si l'éxécution c'est bien passée
    $resultat = $reqPrep->fetchAll();
    //retourner le Club rechercher
    return $resultat;
}
/*
 * Exercice 5 - Question 1
 * Modification d'un club existant
 * @param PDO $bdd
 * @param array $club un tableau avec la clé id
 * @return array|null le club modifié
 */
function updateClub(\PDO $bdd, array $club): ?array {
    dump_var($club, DUMP, '$club dans updateClub');
    $resultSet = NULL;
    $query = sprintf("UPDATE CLUB SET nomClub='%s', adresseClub='%s', cpClub='%s', villeClub='%s' WHERE idClub=%s", $club['nomClub'], $club['adresseClub'], $club['cpClub'], $club['villeClub'], $club['idClub']);
    $reqPrep = $bdd->prepare($query);
    dump_var($reqPrep, DUMP, '$reqPrep dans getOeuvreId');
    $reqPrep->execute();
    $query = sprintf("SELECT * FROM CLUB WHERE idClub=%s", $club['idClub']);
    $res = $bdd->query($query);
    $resultSet = $res->fetchAll()[0];
    //retourner le Club maj
    return $resultSet;

}

/**
 * exercice 6 - Question 1
 * Fait un insert ou un update du club selon la clé id
 * @param PDO $bdd
 * @param array $club
 * @return array|null
 */
function saveClub(PDO $bdd, array $club): ?array
{
    if(isset($club['idClub'])){
        $query = sprintf("SELECT * FROM CLUB WHERE idClub=%s", $club['idClub']);
        $res = $bdd->query($query);
        $res = $res->fetchAll();
        if(isset($res[0])){
            return updateClub($bdd, $club);
        }else{
            return insertClub($bdd, $club);
        }
    }else{
        return insertClub($bdd, $club);
    }
}