<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);
define('DUMP', false);
//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

$error = 0;

//  On s'assure qu'on arrive bien selon la méthode POST
if ('POST' === $_SERVER['REQUEST_METHOD']) {

    dump_var($_POST, DUMP, '$_POST');

    $filters = array(
        'idCompet' => FILTER_VALIDATE_INT,
        'dateDebutCompet' => FILTER_SANITIZE_STRING,
        'dateFinCompet' => FILTER_SANITIZE_STRING,
        'idClubOrganisateur' => FILTER_SANITIZE_STRING
    );
    //  Vérification des types
    $postFiltre = filter_input_array(INPUT_POST, $filters, TRUE);

    dump_var($postFiltre, DUMP, '$postFiltre');
    //Connexion à la BDD
    $bdd = connectBdd($infoBdd);
    if ($bdd) {
        // on fait appel à la fonction save qui permettra de rediriger si c'est une insertion ou une maj
        $repoCompet = new Repositories\CompetitionRepository($bdd);
        $repoCompet->update(new \Entities\Competition($postFiltre));
        dump_var($repoCompet, DUMP, '$club');
    }
    else {
        $error = -3;
    }
}
else {
    $error = -1;
}

//  Redirection vers la liste des sites si DUMP est false
if (!DUMP)
    if($error === 0)
        header("Location: ../pages/listeCompet.php");
    else
        header("Location: ../pages/listeCompet.php");
else
{
    echo'<p>DUMP est true, redirection vers "../listeClubs.php" dévalidée.</p>';
    dump_var($error, DUMP, '$error:');
}