<?php
//  On s'assure qu'on arrive bien selon la méthode POST
if ('POST' === $_SERVER['REQUEST_METHOD']) {



    $filters = array(
        'idCompet' => FILTER_VALIDATE_INT,
        'dateDebutCompet' => FILTER_SANITIZE_STRING,
        'dateFinCompet' => FILTER_SANITIZE_STRING,
        'idClubOrganisateur' => FILTER_SANITIZE_STRING
    );
    //  Vérification des types
    $postFiltre = filter_input_array(INPUT_POST, $filters, TRUE);

    print_r($postFiltre);



    //  Les inclusions nécessaires
    require_once '../../config/appConfig.php';
    require_once '../../src/fonctionsUtiles.php';


//  Par principe, mettez le maximum du code PHP nécessaire ici.
//Connexion à la BDD
    $bdd = connectBdd($infoBdd);

    if ($bdd) {
        $repoCompet = new Repositories\CompetitionRepository($bdd);
        $repoCompet->insert(new \Entities\Competition($postFiltre));
        dump_var($repoCompet,true,"ma competition après insertion");
    } else {
        $repoCompet = null;
    }
    if ($repoCompet == null){
        header('Status: 301 Moved Permanently', true, 301);
        header( "Location: ../pages/listeCompet.php");;
    } else {
        header('Status: 301 Moved Permanently', false, 301);
        header("Location: ../pages/listeCompet.php");
    }

}

?>
</body>
</html>
