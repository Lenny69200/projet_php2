<?php
//  On s'assure qu'on arrive bien selon la méthode POST
if ('POST' === $_SERVER['REQUEST_METHOD']) {



    $filters = array(
        'idClub' => FILTER_VALIDATE_INT,
        'nomClub' => FILTER_SANITIZE_STRING,
        'adresseClub' => FILTER_SANITIZE_STRING,
        'cpClub' => FILTER_SANITIZE_STRING,
        'villeClub' => FILTER_SANITIZE_STRING
    );
    //  Vérification des types
    $postFiltre = filter_input_array(INPUT_POST, $filters, TRUE);



    //  Les inclusions nécessaires
    require_once '../../config/appConfig.php';
    require_once '../../src/fonctionsUtiles.php';


//  Par principe, mettez le maximum du code PHP nécessaire ici.
//Connexion à la BDD
    $bdd = connectBdd($infoBdd);
//Si la connexion réussi alors j'appelle ma fonction getAllClubs sinon $lesClubs est null
    if ($bdd) {
        $lesClubs =insertClub($bdd,$postFiltre);
        dump_var($lesClubs,true,"mon club après insertion");
    } else {
        $lesClubs = null;
    }
if ($lesClubs == null){
    header('Status: 301 Moved Permanently', true, 301);
    header('Location: http://localhost/p2023_1sio_gestion_competition-etudiant-main/public/pages/listeClubs.php');;
} else {
    header('Status: 301 Moved Permanently', false, 301);
    header('Location: http://localhost/p2023_1sio_gestion_competition-etudiant-main/public/pages/listeClubs.php');
}

}

?>
</body>
</html>
