<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

//  Par principe, mettez le maximum du code PHP nécessaire ici.
//Connexion à la BDD
$db = connectBdd($infoBdd);

if ($db)
{
    $repoCompet = new Repositories\CompetitionRepository($db);
    $listeCompet= $repoCompet->getAll();



}
else {
    $listeCompet=null;
}

?>
<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - Projet PHP  7 - Les Competitions </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/style.css">

</HEAD>
<BODY>
<?php
include_once '../inc/header.php';
include_once '../inc/menu.php';
?>
<section id="corps">
    <h1>LISTES DES COMPETITIONS</h1>
    <p>Un petit extrait de notre base de données </p>
    <?php if (!is_null($listeCompet)): ?> <!-- Permet de faire la suite du code uniquement si j'ai récupèrer des données !-->
        <table id='table2'>
            <thead>
            <tr><th>Id</th><th>Date Debut Competition </th><th>Date Fin Competition </th><th>Club Organisateur</th><th>Éditer</th><th>Suprimer</th><th>Pour Voir la rencontre</th></tr>
            </thead>
            <tbody>
            <!-- Vous devez  parcourir votre tableau lesClubs et pour chaque enregistrement l'afficher  dans votre tableau HTML !-->
            <?php
            foreach ($listeCompet as $form):

                ?>
                <tr>
                    <td><?= $form->getIdCompet(); ?></td>
                    <td id="colonneLargeur2"><?= $form->getDateDebutCompet(); ?></td>
                    <td id="colonneLargeur3"><?= $form->getDateFinCompet(); ?></td>
                    <td id="colonneLargeur4"><?= $form->getIdClubOrganisateur(); ?>
                    <td><a href="formEditCompetPOO.php?idCompet=<?= $form->getIdCompet(); ?>"<img src="../img/edit.png" /><img src="../img/edit.png" /></a></td>
                    <td><a href="formDeleteCompetPOO?idCompet=<?= $form->getIdCompet(); ?>" <img src="../img/delete.jpg" /> ❌ </td>
                    <td><a href="=<?= $form->getIdCompet(); ?>"> Pour Voir la rencontre</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Oups... Il semble y avoir eu une erreur!</p>
    <?php endif; ?>
</section>
<?php
include_once '../inc/footer.php';
?>
</body>
</html>