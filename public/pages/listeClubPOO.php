<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

//  Par principe, mettez le maximum du code PHP nécessaire ici.
//Connexion à la BDD
$db = connectBdd($infoBdd);

$repo = new Repositories\ClubRepository($db);

$listeClub = $repo->getAll();

?>
<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - TP PHP Exercice 2 - Recupérer les clubs </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/style.css">

</HEAD>
<BODY>
<?php
include_once '../inc/header.php';
include_once '../inc/menu.php';
?>
<section id="corps">
    <h1>LISTES DES CLUBS</h1>
    <p>Un petit extrait de notre base de données </p>
    <?php if (!is_null($listeClub)): ?> <!-- Permet de faire la suite du code uniquement si j'ai récupèrer des données !-->
        <table id='table2'>
            <thead>
            <tr><th>Id</th><th>Nom du club</th><th>Adresse du club</th><th>Code postal</th><th>Ville</th><th>Éditer</th></tr>
            </thead>
            <tbody>
            <!-- Vous devez  parcourir votre tableau lesClubs et pour chaque enregistrement l'afficher  dans votre tableau HTML !-->
            <?php
            foreach ($listeClub as $form):

                ?>
                <tr>
                    <td><?= $form->getIdClub(); ?></td>
                    <td id="colonneLargeur2"><?= $form->getNomClub(); ?></td>
                    <td id="colonneLargeur2"><?= $form->getAdresseClub(); ?></td>
                    <td><?= $form->getCpClub(); ?></td>
                    <td id="colonneLargeur3"><?= $form->getVilleClub(); ?>
                    <td><a href="formEditCLubPOO.php?idClub=<?= $form->getIdClub(); ?>"> Pour Modifier</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Oups... Il semble y avoir eu une erreur!</p>
    <?php endif; ?>
</section>
<?php
include_once '../inc/footer.php';
?>
</body>
</html>