<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';



//  Par principe, mettez le maximum du code PHP nécessaire ici.
//Connexion à la BDD
$bdd = connectBdd($infoBdd);
if ($bdd) {
    $repoCompet = new Repositories\CompetitionRepository($bdd);
    $listeCompet= $repoCompet->getAll($bdd);
} else {
    $listeCompet= null;
}
?>
<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - Projet PHP </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/style.css">

</HEAD>
<BODY>
<?php
include_once '../inc/header.php';
include_once '../inc/menu.php';
?>

<section id="corps">
    <h1> Ajouter Une Competitions </h1> <!--A modifier pour la partie Optimisation !-->
    </header>

    <form method="post" action="../traits/traitInsertCompet.php">
        <input type="hidden" id="idCompet" name="idCompet" value=" <!--Permet de garder l'id quand on envoie les données du formulaire !-->
        <div>
            <label for="nomClub">Date Debut Compet :</label><br/>
        <input type="text" id="dateDebutCompet" placeholder="dateDebutCompet" name="dateDebutCompet" value="" size="40">
        </div>
        <div>
            <label for="DateFinCompet">Date Fin Compet :</label><br/>
            <input type="text"id="dateFinCompet" placeholder="dateFinCompet" name="dateFinCompet" size="100"  value="" required="required">
        </div>
        <div>
            <label for="idClubOrganisateur">CLub Organisateur :</label><br/>
            <input type="text"id="idClubOrganisateur" placeholder="idClubOrganisateur" name="idClubOrganisateur" size="20" value="" required="required">
            <br/>
        <br/>
        <div class="form-group">
            <button type="submit"> Ajouter</button>
        </div>

    </form>

</section>

<div class="footer-container">
    <?php include_once '../inc/footer.php'; ?>
</div>

<script src="js/kickstart.js"></script> <!-- KICKSTART -->
<script src="js/main.js"></script>
</body>
</html>