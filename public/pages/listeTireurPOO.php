<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

//  Par principe, mettez le maximum du code PHP nécessaire ici.
//Connexion à la BDD
 $db = connectBdd($infoBdd);

 if ($db)
 {
     $repoTireur = new Repositories\TireurRepository($db);
     $listeTireur = $repoTireur->getAll();
     $repoClub = new Repositories\ClubRepository($db);
     $repoNivTireur = new Repositories\NiveauTireurRepository($db);


 }
else {
    $listeTireur=null;
}

?>
<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - TP PHP Exercice 7 - Recupérer les Tireurs </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/style.css">

</HEAD>
<BODY>
<?php
include_once '../inc/header.php';
include_once '../inc/menu.php';
?>
<section id="corps">
    <h1>LISTES DES Tireurs</h1>
    <p>Un petit extrait de notre base de données </p>
    <?php if (!is_null($listeTireur)): ?> <!-- Permet de faire la suite du code uniquement si j'ai récupèrer des données !-->
        <table id='table2'>
            <thead>
            <tr><th>Id</th><th>Nom </th><th>Prenoms </th><th>Date de naissance</th><th>Numeros de License</th><th>Sexe</th>
                <th>Poids</th><th>ID CLUB</th><th>Id NiveauxTireur </th><th>Éditer</th></tr>
            </thead>
            <tbody>
            <!-- Vous devez  parcourir votre tableau lesClubs et pour chaque enregistrement l'afficher  dans votre tableau HTML !-->
            <?php
            foreach ($listeTireur as $form):
                $club=$repoClub->getById($form->getIdClub());
                $NivTireur=$repoNivTireur->getById($form->getIdNivTireur());
                ?>
                <tr>
                    <td><?= $form->getIdTireur(); ?></td>
                    <td id="colonneLargeur2"><?= $form->getNomTireur(); ?></td>
                    <td id="colonneLargeur2"><?= $form->getPrenomTireur(); ?></td>
                    <td><?= $form->getDateNaissTireur(); ?></td>
                    <td id="colonneLargeur3"><?= $form->getNumLicenceTireur(); ?>
                    <td id="colonneLargeur3"><?= $form->getSexeTireur(); ?>
                    <td id="colonneLargeur3"><?= $form->getPoidsTireur(); ?>
                    <td id="colonneLargeur3"><?= $club->getNomClub(); ?>
                    <td id="colonneLargeur3"><?=  $NivTireur->getLibNivTireur(); ?>
                    <td><a href="formEditCLubPOO.php?idClub=<?= $form->getIdTireur(); ?>"> Pour Modifier</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Oups... Il semble y avoir eu une erreur!</p>
    <?php endif; ?>
</section>
<?php
include_once '../inc/footer.php';
?>
</body>
</html>