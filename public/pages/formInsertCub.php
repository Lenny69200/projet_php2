<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';


//  Par principe, mettez le maximum du code PHP nécessaire ici.
//Connexion à la BDD
$bdd = connectBdd($infoBdd);
//Si la connexion réussi alors j'appelle ma fonction getAllClubs sinon $lesClubs est null
if ($bdd) {
    $lesClubs  = getAllClubs($bdd);
} else {
    $lesClubs = null;
}
?>
<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - TP PHP Partie 2 </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/style.css">

</HEAD>
<BODY>
<?php
include_once '../inc/header.php';
include_once '../inc/menu.php';
?>

<section id="corps">
    <h1> Ajouter un club </h1> <!--A modifier pour la partie Optimisation !-->
    </header>

    <form method="post" action="../traits/traitInsertClub.php">
        <input type="hidden" id="idClub" name="idClub" value=" <!--Permet de garder l'id quand on envoie les données du formulaire !-->
        <div>
            <label for="nomClub">Nom du club :</label><br/>
            <input type="text" id="nomClub" placeholder="nom du club" name="nomClub" value="" size="40">
        </div>
        <div>
            <label for="adresseClub">Adresse du club :</label><br/>
            <input type="text"id="adresseClub" placeholder="Adresse du club" name="adresseClub" size="100"  value="" required="required">
        </div>
        <div>
            <label for="cpClub">Code postal du club :</label><br/>
            <input type="text"id="cpClub" placeholder="Code postal du club" name="cpClub" size="20" value="" required="required">
        </div>
        <div>
            <label for="villeClub">Ville du club :</label><br/>
            <input type="text" id="villeClub" placeholder="Ville du club" name="villeClub" size="40" value="" required="required">
        </div>
        <br/>
        <div class="form-group">
            <button type="submit"> Ajouter</button>
        </div>

    </form>

</section>

<div class="footer-container">
    <?php include_once '../inc/footer.php'; ?>
</div>

<script src="js/kickstart.js"></script> <!-- KICKSTART -->
<script src="js/main.js"></script>
</body>
</html>