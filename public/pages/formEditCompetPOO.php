<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';

//  Par principe, mettez le maximum du code PHP nécessaire ici.

$Compet = null;
$idCompet=null;

//Récupération de l'idClub envoyée en GET
$edit = false;
if(isset($_GET['idCompet'])){
    $edit = true;
    $idCompet = (int) filter_input(INPUT_GET,"idCompet",FILTER_VALIDATE_INT);
}
//Si on a un idClub qui a été passé en get à la page on se connecte à la BDD
if($idCompet !== false && !is_null($idCompet)) {
    //Connexion à la BDD
    $db = connectBdd($infoBdd);

    $repo = new Repositories\CompetitionRepository($db);
    if ($db) {
        $Compet = $repo->getById($idCompet);
    }
}

?>
<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - TP PHP Partie 2 </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/style.css">

</HEAD>
<BODY>
<?php
include_once '../inc/header.php';
include_once '../inc/menu.php';
?>

<section id="corps">
    <h1> Modifier une Competition </h1> <!--A modifier pour la partie Optimisation !-->
    </header>

    <form method="post" action="../traits/traitDeleteCompet.php">
        <input type="hidden" id="idCompet" name="idCompet" value="<?php if($edit){echo $Compet->getIdCompet();} ?>" /> <!--Permet de garder l'id quand on envoie les données du formulaire !-->
        <div>
            <label for="nomClub">dateDebutCompet :</label><br/>
            <input type="text" id="dateDebutCompet" placeholder="dateDebutCompet" name="dateDebutCompet" value="<?php if($edit){echo $Compet->getDateDebutCompet();} ?>" size="40">
        </div>
        <div>
            <label for="dateFinCompet">dateFinCompet :</label><br/>
            <input type="text"id="dateFinCompet" placeholder="dateFinCompet" name="dateFinCompet" size="100"  value="<?php if($edit){echo $Compet->getDateFinCompet();} ?>" required="required">
        </div>
        <div>
            <label for="idClubOrganisateur">idClubOrganisateur :</label><br/>
            <input type="text"id="idClubOrganisateur" placeholder="idClubOrganisateur" name="idClubOrganisateur" size="20" value="<?php if($edit){echo $Compet->getIdClubOrganisateur();} ?>" required="required">
        </div>
        <br/>
        <div class="form-group">
            <button type="submit"> 'Modifier'</button>
        </div>

    </form>

</section>

<div class="footer-container">
    <?php include_once '../inc/footer.php'; ?>
</div>

<script src="js/kickstart.js"></script> <!-- KICKSTART -->
<script src="js/main.js"></script>
</body>
</html>

