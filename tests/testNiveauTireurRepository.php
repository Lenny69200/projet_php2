<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

//  Pour avoir la configuration et les informations de connexion dans $infoBdd
require_once '../config/appConfig.php';
//  Pour utiliser les fonctions
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);
dump_var($db, DUMP, 'Objet PDO:');

if (!is_null($db)) {
    $repo = new Repositories\NiveauTireurRepository($db);

    $res = $repo->getAll();
    dump_var($res, DUMP, 'Liste des clubs :');

    $tab = array (
        'libNivTireur' => 'super genie'

    );

    $unNiv = new Entities\NiveauTireur($tab);

    $res = $repo->insert($unNiv);
    dump_var($res, DUMP, "ajout d'un Niveau");

    $tab = array (
        'idNivTireur' => 2,
        'libNivTireur' => 'Lenny'

    );
    $unNiv = new Entities\NiveauTireur($tab);

    $res = $repo->update($unNiv);
    dump_var($res, DUMP, "mise à jour d'un club");

    $res = $repo->getById(1);
    dump_var($res, DUMP, "information du club numéro 1");

    $res = $repo->getAll();
    dump_var($res, DUMP, 'Liste des clubs :');
}