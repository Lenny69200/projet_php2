<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

require_once '../config/appConfig.php';

use Entities\Competition;

echo '<h1>Instanciation par défaut</h1>';
$obj = new Competition();
dump_var($obj, DUMP, 'Competition par défaut:');

$tab = array (
    'idCompet'=>1,
    'DateDebutCompet' => '2021-02-01',
    'DateFinCompet' => '2020-05-07 ',
    'idClubOrganisateur' => '2 '

);
echo '<h1>Instanciation avec toutes les infos </h1>';
$obj = new Competition($tab);
dump_var($obj, DUMP, 'Competition avec toutes les valeurs:');

echo '<h1>Modification du numéro </h1>';
$obj->setIdCompet(3);
dump_var($obj, DUMP, 'Competition modifier:');

echo '<h1>Modification du numéro </h1>';
$obj->setIdClubOrganisateur(5);
dump_var($obj, DUMP, 'Club organisateur modifier:');
