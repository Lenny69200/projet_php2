<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

require_once '../config/appConfig.php';

use Entities\Tireur;

echo '<h1>Instanciation par défaut</h1>';
$obj = new Tireur();
dump_var($obj, DUMP, 'Tireur par défaut:');

$tab = array (
    'idTireur'=>1,
    'nomTireur'=>"Rick",
    'prenomTireur'=>"Morty",
    'dateNaissTireur' => '1970',
    'numLicenceTireur' => 34,
    'sexeTireur' => 'F',
    'poidsTireur' => 80.0

);
echo '<h1>Instanciation avec toutes les infos </h1>';
$obj = new Tireur($tab);
dump_var($obj, DUMP, 'Tireur avec toutes les valeurs:');

echo '<h1>Modification du numéro </h1>';
$obj->setIdTireur(2);
$obj->setNomTireur("Maximus");
dump_var($obj, DUMP, 'Tireur modifier:');


