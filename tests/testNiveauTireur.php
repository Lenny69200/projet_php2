<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

require_once '../config/appConfig.php';

use Entities\NiveauTireur;

echo '<h1>Instanciation par défaut</h1>';
$obj = new NiveauTireur();
dump_var($obj, DUMP, 'Niveaux par défaut:');

$tab = array (
    'idNivTireur'=>1,
    'libNivTireur' => 'Debutant'

);
echo '<h1>Instanciation avec toutes les infos </h1>';
$obj = new NiveauTireur($tab);
dump_var($obj, DUMP, 'Niveaux avec toutes les valeurs:');

echo '<h1>Modification du numéro </h1>';
$obj->setIdNivTireur(2);
dump_var($obj, DUMP, 'Niveaux modifier:');

