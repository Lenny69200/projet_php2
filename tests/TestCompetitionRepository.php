<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

//  Pour avoir la configuration et les informations de connexion dans $infoBdd
require_once '../config/appConfig.php';
//  Pour utiliser les fonctions
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);
dump_var($db, DUMP, 'Objet PDO:');

if (!is_null($db)) {
    $repo = new Repositories\CompetitionRepository($db);

/*    $res = $repo->getAll();
    dump_var($res, DUMP, 'Liste des Competitions :');

    $tab = array (
        'DateDebutCompet' => '2011-06-10',
        'DateFinCompet' => ' 2022-03-10',
        'idClubOrganisateur' => 2

    );

    $uneCompet = new Entities\Competition($tab);

    $res = $repo->insert($uneCompet);
    dump_var($res, DUMP, "ajout d'une Competitions ");

    $tab = array (
        'idCompet' => 2,
        'DateDebutCompet' => '2021-06-10',
        'DateFinCompet' => '2024-08-12',
    'idClubOrganisateur' => 3

    );
    $unNiv = new Entities\Competition($tab);

    $res = $repo->update($uneCompet);
    dump_var($res, DUMP, "mise à jour d'une Competition");

    $res = $repo->getById(1);
    dump_var($res, DUMP, "information de la Compet numéro 1");

    $res = $repo->getAll();
    dump_var($res, DUMP, 'Liste des Competitions :');

*/

    $res = $repo->delete(4);
    dump_var($res, DUMP, 'Liste des Competitions :');
}