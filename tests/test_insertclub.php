<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);
//  Pour forcer les dumps pendant les tests
define('DUMP', true);

//  Pour avoir la configuration et les informations de connexion dans $infoBdd
//  Basculez la constante DUMP de appConfig à true pour les tests.
require_once '../config/appConfig.php';
//  Pour utiliser les fonctions
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);
dump_var($db, DUMP, 'Objet PDO:');

if (!is_null($db)) {
    $datasClub = [
        'nomClub' => 'Club_'. rand(1,99),
        'adresseClub' => rand(1,20).'rue des clubs de boxe',
        'cpClub' => rand(69000,69100),
        'villeClub' => 'Ville fictive nouvelle version'
    ];
    dump_var($datasClub, DUMP, '$datasClub:');

    $res = insertClub($db, $datasClub);
    dump_var($res, DUMP, '$res après insert de pioussos:');
} else {
    echo '<h1>Erreur de création de la connexion $db</h1>';
}