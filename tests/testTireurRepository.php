<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

//  Pour avoir la configuration et les informations de connexion dans $infoBdd
require_once '../config/appConfig.php';
//  Pour utiliser les fonctions
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);
dump_var($db, DUMP, 'Objet PDO:');

if (!is_null($db)) {
    $repo = new Repositories\TireurRepository($db);

    $res = $repo->getAll();
       dump_var($res, DUMP, 'Liste des Tireurs :');

    $tab = array (
        'idTireur' => 2,
        'nomTireur' => 'Super ',
        'prenomTireur' => 'Abdou ',
        'dateNaissTireur' => null,
        'numLicenceTireur' => 1,
        'sexeTireur' => 'F',
        'poidsTireur' => 30.0,
        'idClub' => 2,
    'idNivTireur' => 3
    );

    $unTireur = new Entities\Tireur($tab);

    $res = $repo->insert($unTireur);
    dump_var($res, DUMP, "ajout d'un Tireur");

    $tab = array (
        'idTireur' => 2,
        'nomTireur' => 'Bonus',
        'prenomTireur' => 'Loris ',
        'dateNaissTireur' => null,
        'numLicenceTireur' => 1,
        'sexeTireur' => 'F',
        'poidsTireur' => 30.0,
         'idClub' => 2,
    'idNivTireur' => 3

    );
    $unTireur = new Entities\Tireur($tab);

    $res = $repo->update($unTireur);
    dump_var($res, DUMP, "mise à jour d'un club");

    $res = $repo->getById(1);
    dump_var($res, DUMP, "information du club numéro 1");

    $res = $repo->getAll();
    dump_var($res, DUMP, 'Liste des clubs :');
}